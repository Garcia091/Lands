﻿

namespace Lands_1.Infrastructure
{
    using ViewModels;
    public class IntanceLocator
    {
        #region Properties
        public MainViewModel Main
        {
            get;
            set;
        }
        #endregion
        #region Constructor
        public IntanceLocator()
        {
            this.Main = new MainViewModel(); //nos sirve para ligra Login con ViewModel
        }
        #endregion
    }
}
